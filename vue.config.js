module.exports = {
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.ical$/i,
          use: 'raw-loader',
        },
      ],
    }
  }
}