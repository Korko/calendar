interface ICalNode {
    [key: string]: string | ICalList | ICalNode;// null | ICalNode for parent
}
type ICalList = Array<ICalNode>;

export default class ICalReader {
    public root: ICalNode;

    constructor(root: ICalNode) {
        this.root = root;
    }

    static parse(obj: string): ICalReader {
        var lines: string[] = obj.split("\r\n");

        var root: ICalNode = {};
        root.parent = root;

        var lastElt: ICalNode = root;
        var lastAttribute: string = '';

        lines.forEach(line => {
            if(line[0] === ' ' || line[0] === "\t") {
                lastElt[lastAttribute] = (lastElt[lastAttribute] || '') + line.substr(1);
            } else {
                var name: string, value: string;
                [name, value] = line.split(':');console.debug(line, name, value);
                if(name === "BEGIN") {
                    var newElt: ICalNode = {
                        parent: lastElt
                    };

                    lastElt[value] = (lastElt[value] || []);
                    (<ICalList>lastElt[value]).push(newElt);
                    lastElt = newElt;
                } else if(name === "END") {
                    lastElt = <ICalNode>lastElt.parent;
                } else {
                    lastAttribute = name;
                    lastElt[name] = value;
                }
            }
        });

        return new ICalReader(root);
    }
}